# 10 Jahre alter Linux-PC

    OpenSSL 1.1.1k  25 Mar 2021
    built on: Thu Mar 25 16:27:51 2021 UTC
    options:bn(64,64) rc4(16x,int) des(int) aes(partial) idea(int) blowfish(ptr) 
    compiler: gcc -fPIC -pthread -m64 -Wa,--noexecstack -march=x86-64
    -mtune=generic -O2 -pipe -fno-plt -Wa,--noexecstack -D_FORTIFY_SOURCE=2
    -march=x86-64 -mtune=generic -O2 -pipe -fno-plt
    -Wl,-O1,--sort-common,--as-needed,-z,relro,-z,now -DOPENSSL_USE_NODELETE
    -DL_ENDIAN -DOPENSSL_PIC -DOPENSSL_CPUID_OBJ -DOPENSSL_IA32_SSE2
    -DOPENSSL_BN_ASM_MONT -DOPENSSL_BN_ASM_MONT5 -DOPENSSL_BN_ASM_GF2m -DSHA1_ASM
    -DSHA256_ASM -DSHA512_ASM -DKECCAK1600_ASM -DRC4_ASM -DMD5_ASM -DAESNI_ASM
    -DVPAES_ASM -DGHASH_ASM -DECP_NISTZ256_ASM -DX25519_ASM -DPOLY1305_ASM -DNDEBUG
    -D_FORTIFY_SOURCE=2

                                          sign    verify    sign/s verify/s
     160 bits ecdsa (secp160r1)         0.0005s   0.0004s   2003.0   2242.2
     192 bits ecdsa (nistp192)          0.0006s   0.0005s   1639.8   1893.2
     224 bits ecdsa (nistp224)          0.0001s   0.0003s   8429.1   3956.1
     256 bits ecdsa (nistp256)          0.0001s   0.0002s  14826.7   4702.2
     384 bits ecdsa (nistp384)          0.0025s   0.0020s    395.0    505.2
     521 bits ecdsa (nistp521)          0.0008s   0.0015s   1322.4    671.6
     163 bits ecdsa (nistk163)          0.0007s   0.0014s   1411.4    714.2
     233 bits ecdsa (nistk233)          0.0010s   0.0019s   1020.7    515.6
     283 bits ecdsa (nistk283)          0.0020s   0.0039s    504.8    256.7
     409 bits ecdsa (nistk409)          0.0043s   0.0083s    234.3    121.1
     571 bits ecdsa (nistk571)          0.0089s   0.0178s    111.9     56.1
     163 bits ecdsa (nistb163)          0.0008s   0.0015s   1332.5    675.5
     233 bits ecdsa (nistb233)          0.0010s   0.0020s    986.4    497.8
     283 bits ecdsa (nistb283)          0.0022s   0.0043s    461.7    232.5
     409 bits ecdsa (nistb409)          0.0046s   0.0090s    217.7    110.7
     571 bits ecdsa (nistb571)          0.0103s   0.0199s     97.0     50.1

     256 bits ecdsa (brainpoolP256r1)   0.0010s   0.0009s    970.9   1070.2

     256 bits ecdsa (brainpoolP256t1)   0.0010s   0.0009s    965.2   1126.6
     384 bits ecdsa (brainpoolP384r1)   0.0025s   0.0021s    398.7    473.7
     384 bits ecdsa (brainpoolP384t1)   0.0025s   0.0020s    400.1    511.4
     512 bits ecdsa (brainpoolP512r1)   0.0043s   0.0034s    234.2    297.7
     512 bits ecdsa (brainpoolP512t1)   0.0042s   0.0032s    236.0    315.5

