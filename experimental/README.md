# Test Element doppelt in Liste

Ich bekomme eine Liste von NUM Elementen und möchte testen,
ob es doppelte Elemente gibt:

Erst mit 1.000.000 Elementen

    $ ./elements.py
    Anzahl der Elemente= 1000000 Log10= 6.0
    build-in SHA-256
    100% (1000000 of 1000000) |#######################################| Elapsed Time: 0:00:04 Time:  0:00:04
    Dauer 8.029027700424194 Sekunden
    Size of set: 33554648 MiB: 32.000205993652344
    xxhash 128
    100% (1000000 of 1000000) |#######################################| Elapsed Time: 0:00:03 Time:  0:00:03
    Dauer 3.782954216003418 Sekunden
    Size of set: 33554648 MiB: 32.000205993652344

Jetzt mit 10.000.000 Elementen

    $ ./elements.py
    Anzahl der Elemente= 10000000 Log10= 7.0
    build-in SHA-256
    100% (10000000 of 10000000) |#####################################| Elapsed Time: 0:00:46 Time:  0:00:46
    Dauer 49.85752034187317 Sekunden
    Size of set: 268435672 MiB: 256.00020599365234
    xxhash 128
    100% (10000000 of 10000000) |#####################################| Elapsed Time: 0:00:38 Time:  0:00:38
    Dauer 39.003411531448364 Sekunden
    Size of set: 268435672 MiB: 256.00020599365234

Jetzt mit 100.000.000 Elementen

    $ ./elements.py
    Anzahl der Elemente= 100000000 Log10= 8.0
    build-in SHA-256
    100% (100000000 of 100000000) |###################################| Elapsed Time: 0:16:34 Time:  0:16:34
    Dauer 998.4947154521942 Sekunden
    Size of set: 4294967512 MiB: 4096.000205993652
    xxhash 128
    100% (100000000 of 100000000) |###################################| Elapsed Time: 0:08:31 Time:  0:08:31
    Dauer 516.6941502094269 Sekunden
    Size of set: 4294967512 MiB: 4096.000205993652

Die Dauer ist so hoch, weil der Laptop bei ca. 60% irgendwelche
Programme/Speicherbereiche in den Swap-Space ausgelagert hat um
RAM frei zu geben. Da hat das Programm warten müssen.


## Testlauf 2

Mit 10^6:

    $ ./elements.py
    Anzahl der Elemente= 1000000 Log10= 6.0
    build-in SHA-256
    100% (1000000 of 1000000) |#######################################| Elapsed Time: 0:00:08 Time:  0:00:08
    Dauer 12.008878231048584 Sekunden
    Size of set: 33554648 MiB: 32.000205993652344
    xxhash 128
    100% (1000000 of 1000000) |#######################################| Elapsed Time: 0:00:06 Time:  0:00:06
    Dauer 6.783603191375732 Sekunden
    Size of set: 33554648 MiB: 32.000205993652344
    build-in Hashfunktion
    100% (1000000 of 1000000) |#######################################| Elapsed Time: 0:00:04 Time:  0:00:04
    Dauer 4.2450573444366455 Sekunden
    Size of set: 33554648 MiB: 32.000205993652344

Mit 10^7:

    $ ./elements.py
    Anzahl der Elemente= 10000000 Log10= 7.0
    build-in SHA-256
    100% (10000000 of 10000000) |#####################################| Elapsed Time: 0:00:49 Time:  0:00:49
    Dauer 52.67372179031372 Sekunden
    Size of set: 268435672 MiB: 256.00020599365234
    xxhash 128
    100% (10000000 of 10000000) |#####################################| Elapsed Time: 0:00:38 Time:  0:00:38
    Dauer 38.534501791000366 Sekunden
    Size of set: 268435672 MiB: 256.00020599365234
    build-in Hashfunktion
    100% (10000000 of 10000000) |#####################################| Elapsed Time: 0:00:23 Time:  0:00:23
    Dauer 23.989258766174316 Sekunden
    Size of set: 268435672 MiB: 256.00020599365234

