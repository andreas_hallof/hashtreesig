#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Testet in einer großen Liste, ob Elemente doppelt
vorhanden sind. Als Beispiel-Elemente erzeuge ich
mir dynamisch die Binär-Repräsentation der Zahlen 0 bis 
NUM-1.

"""

import hashlib, math, sys, time, xxhash

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from progressbar import progressbar

if __name__ == '__main__':

    NUM = 10_000_000
   
    print("Anzahl der Elemente=", NUM, "Log10=", math.log10(NUM))
    print("build-in SHA-256")
    my_set = set()

    start = time.time()
    for i in progressbar(range(0,NUM)):
        x = i.to_bytes(length=64, byteorder='big', signed=False)
        h = hashlib.sha256(x).digest() 
        if not (h in my_set):
            my_set.add(h)
        else:
            sys.exit("UHIIII should never happen")
    
    print("Dauer", time.time()-start, "Sekunden")
    x = sys.getsizeof(my_set)
    print("Size of set:", x, "MiB:", x/1024.0/1024.0)

    ## ist ca. 3.6 mal so langsam wie die hashlib-sha256-Funktion
    #    print("cryptography SHA-256")
    #    my_set = set()
    #
    #    start = time.time()
    #    for i in progressbar(range(0,NUM)):
    #        x = i.to_bytes(length=64, byteorder='big', signed=False)
    #        mh = hashes.Hash(hashes.SHA256())
    #        mh.update(x)
    #        h = mh.finalize()
    #        if not (h in my_set):
    #            my_set.add(h)
    #        else:
    #            sys.exit("UHIIII should never happen")
    #    
    #    print("Dauer", time.time()-start, "Sekunden")
    #    x = sys.getsizeof(my_set)
    #    print("Size of set:", x, "MiB:", x/1024.0/1024.0)
    #

    print("xxhash 128")
    my_set = set()

    start = time.time()
    for i in progressbar(range(0,NUM)):
        x = i.to_bytes(length=64, byteorder='big', signed=False)
        h = xxhash.xxh3_128(x).digest() 
        if not (h in my_set):
            my_set.add(h)
        else:
            sys.exit("UHIIII should never happen")
    
    print("Dauer", time.time()-start, "Sekunden")
    x = sys.getsizeof(my_set)
    print("Size of set:", x, "MiB:", x/1024.0/1024.0)

    print("build-in Hashfunktion")
    my_set = set()

    start = time.time()
    for i in progressbar(range(0,NUM)):
        x = i.to_bytes(length=64, byteorder='big', signed=False)
        if not (x in my_set):
            my_set.add(x)
        else:
            sys.exit("UHIIII should never happen")
    
    print("Dauer", time.time()-start, "Sekunden")
    x = sys.getsizeof(my_set)
    print("Size of set:", x, "MiB:", x/1024.0/1024.0)

