#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import hashlib, time

print("Zufallsdaten erhalten")
# Ich möchte bewusst die Daten erst beim mir im Speicher 
# haben und dann den Hashwert berechnen.

my_len = 2**(10+10+10)
print("Ich nehme", my_len, "Bytes")
start = time.time()
with open("/dev/urandom", "rb") as my_urandom:
    my_data = my_urandom.read(my_len)
print("Dauer", time.time()-start, "Sekunden")

print("Start der Hashwertberechnung")
start = time.time()
hex_wert = hashlib.sha256(my_data).hexdigest()
print("Dauer", time.time()-start, "Sekunden;", "Hashwert =", hex_wert)

