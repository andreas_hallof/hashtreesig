#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

"""
Kurze Beispielimplementierung einer Merkle-Hashbaum-ECDSA-Signatur

Die Signaturen werden als JSON Web Signature kodiert.
"""

import os, json, hashlib, math, secrets, sys, time
from base64 import b64encode, b64decode

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.asymmetric import utils

class HashTree:
    def __init__(self):
        self.closed = False
        self.leafs = []
        self.search_dict = dict()

    def add(self, hash_value : bytearray):
        "Fügt den Hashwert einer neuen Nachrichten in den Hashbaum ein."
        
        if len(hash_value) != 32:
            raise ValueError("sha-256 value?")
        self.leafs.append(hash_value)
        self.search_dict[hash_value] = len(self.leafs) - 1
        # der Nachbarknoten soll ein Zufallswert sein
        self.leafs.append(secrets.token_bytes(32))

        return True

    def sign(self):
        """
        1) Zunächst wird der Hashbaum so erweitert, dass er ein Binärbaum 
           ist (Dummy-Elemente werden ergänzt).
        2) Anschließend werden die inneren Knoten-Elemente berechnet und 
           ebenfalls der Wurzelknoten.  
        3) Der Wurzelknoten wird mit ECDSA signiert.
        """

        if self.closed:
            raise ValueError("already signed!")

        # ggf. Hashbaum durch Zunahme von Dummy-Elementen zu einem 
        # vollständigen Binärbaum erweitern
        target_power_of_two = math.ceil( math.log2(len(self.leafs)) )
        nr_missing = 2**target_power_of_two - len(self.leafs)
        self.leafs += [ secrets.token_bytes(32) for i in range(0, nr_missing) ]

        # jetzt alle Knoten des Hashbaums berechnen (Hashwert-Verknüpfung)
        self.nodes = [ self.leafs ]
        while len(self.nodes[-1]) != 1:
           neu = []
           for i in range(0, len(self.nodes[-1]), 2):
               neu.append(hashlib.sha256(self.nodes[-1][i] + self.nodes[-1][i+1]).digest())
           #print(len(neu))
           self.nodes.append(neu)

        # nun kommt die ECDSA-Signatur über das Wurzel-Element des Hashbaums
        with open("pki/signer_cert.der", "rb") as cert_file:
            self.cert = b64encode(cert_file.read()).decode()
        with open("pki/signing_key.pem", "rb") as key_file:
            serialized_private_key=key_file.read()
        private_key = serialization.load_pem_private_key(serialized_private_key,
                                password=None, backend=default_backend() )
        self.ecdsa_signature = private_key.sign(self.nodes[-1][0],
                                ec.ECDSA(utils.Prehashed(hashes.SHA256())))
        self.closed = True

        return True

    def signature(self, hash_value : bytearray):
        if not self.closed:
            raise ValueError("der HashTree ist noch nicht signiert worden")
        if hash_value not in self.search_dict:
            raise ValueError("hash value not in tree")

        offset = self.search_dict[hash_value]
        assert self.nodes[0][offset] == hash_value
        hash_array = [ b64encode(self.leafs[offset+1]).decode() ]
        offset = offset//2
        for i in range(1, len(self.nodes)-1):
            #print("i=", i)
            if i % 2 == 1:
                a = b64encode(self.nodes[i][offset]).decode()
                hash_array += [ a ]
            else:
                a = b64encode(self.nodes[i][offset]).decode()
                hash_array += [ "-" + a ]
            offset = offset//2

        sig = { "ht_path" : hash_array,
                "ecdsa_sig" : b64encode(self.ecdsa_signature).decode() }

        return b64encode(json.dumps(sig).encode())

    def signature_header(self):
        """Gibt den JWS-Header inkl. Zertifikat (= x5c-Element) des
        Signierenden zurück"""

        if not self.closed:
            raise ValueError("der HashTree ist noch nicht signiert worden")

        header = { "alg" : "HTES256", "x5c" : self.cert }
        return b64encode(json.dumps(header).encode())


### Jetzt mal ganz viele Nachrichten in den Hashbaum zum Signieren aufnehmen
#TEST_NR = 3_000_000
TEST_NR = 3

myht = HashTree()

print(f"Ich erzeuge ad-hoc {TEST_NR} Nachrichten ...")
start = time.time()
for message_number in range(1, TEST_NR + 1):
    message_to_sign = f"Hallo, ich bin Nachricht Nr. {message_number}.".encode()
    myht.add(hashlib.sha256(message_to_sign).digest())
print("Dauer", time.time()-start)

print("Jetzt geht es erst los mit der eigentlichen Erstellung des\n" + \
      "Hashbaums und der Signaturerstellung")
### Jetzt den ganzen Hashbaum mit genau einer ECDSA-Signatur signieren
start = time.time()
myht.sign()
print("Dauer", time.time()-start)

### Toll, jetzt sehen wie uns mal eine Nachricht + Signatur als
### JSON Web Signature an
message = f"Hallo, ich bin Nachricht Nr. {TEST_NR}.".encode()
print("Beispiel für die JSON Web Signature der Nachricht '" + message.decode() + "'")
message_hash = hashlib.sha256(message).digest()

json_web_signature = myht.signature_header() + b'.' + b64encode(message) + \
            b'.' +  myht.signature(message_hash)
print(json_web_signature)

print("Header:\n", b64decode(myht.signature_header()))
print("Signatur am Ende (3. Teil der JWS):\n", 
            b64decode(myht.signature(message_hash)))

